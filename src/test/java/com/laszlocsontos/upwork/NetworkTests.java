package com.laszlocsontos.upwork;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by lcsontos on 11/1/17.
 */
public class NetworkTests {

    @Nested
    class CreationTests {

        @Test
        void testCreate() {
            new Network(1);
        }

        @Test
        void testCreate_withZero() {
            assertThrows(IllegalArgumentException.class, () -> new Network(0));
        }

        @Test
        void testCreate_withNegative() {
            assertThrows(IllegalArgumentException.class, () -> new Network(-1));
        }

    }

    @Nested
    class ConnectTests {

        Network testNetwork = createTestNetwork();

        @Test
        void testConnect() {
            testNetwork.connect(1, 8);
        }

        @Test
        void testConnect_withNode1OutOfBounds() {
            assertThrows(IllegalArgumentException.class, () -> testNetwork.connect(0, 8));
        }

        @Test
        void testConnect_withNode2OutOfBounds() {
            assertThrows(IllegalArgumentException.class, () -> testNetwork.connect(1, 9));
        }

    }

    @Nested
    class QueryTests {

        Network testNetwork = createTestNetwork();

        @Test
        void testQuery_withConnectedDirectly() {
            assertConnected(1, 2);
            assertConnected(1, 6);
            assertConnected(2, 4);
            assertConnected(2, 6);
            assertConnected(5, 8);
        }

        @Test
        void testQuery_withConnectedInDirectly() {
            assertConnected(1, 4);
            assertConnected(4, 1);
        }

        @Test
        void testQuery_withDisconnected() {
            assertDisconnected(1, 8);
            assertDisconnected(1, 5);
        }

        @Test
        void testQuery_withNode1OutOfBounds() {
            assertThrows(IllegalArgumentException.class, () -> testNetwork.query(0, 8));
        }

        @Test
        void testConnect_withNode2OutOfBounds() {
            assertThrows(IllegalArgumentException.class, () -> testNetwork.query(1, 9));
        }

        private void assertConnected(int start, int target) {
            assertTrue(
                    testNetwork.query(start, target),
                    start + " and " + target + " should have been connected"
            );
        }

        private void assertDisconnected(int start, int target) {
            assertFalse(
                    testNetwork.query(start, target),
                    start + " and " + target + " should have been connected"
            );
        }

    }

    private Network createTestNetwork() {
        Network network = new Network(8);

        network.connect(1, 2);
        network.connect(1, 6);
        network.connect(2, 4);
        network.connect(2, 6);
        network.connect(5, 8);

        return network;
    }

}
