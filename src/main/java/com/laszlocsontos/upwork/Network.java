package com.laszlocsontos.upwork;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Created by lcsontos on 11/1/17.
 */
public class Network {

    private final Node[] nodes;

    /**
     * Creates a network of {@code numNodes} nodes.
     *
     * @param numNodes a positive integer value indicating the number of nodes in the network
     */
    public Network(int numNodes) {
        if (numNodes <= 0) {
            throw new IllegalArgumentException("numNodes must be a positive integer");
        }

        nodes = IntStream
                .range(0, numNodes)
                .mapToObj(Node::new)
                .toArray(Node[]::new);
    }

    /**
     * Connects {@code node1} and {@code node2}.
     *
     * @param nodeId1 number of node #1
     * @param nodeId2 number of node #2
     * @throws IllegalArgumentException if {@code nodeId1} or {@code nodeId2} is out of bounds
     */
    public void connect(int nodeId1, int nodeId2) {
        checkBounds(nodeId1, nodeId2);

        Node node1 = lookup(nodeId1);
        Node node2 = lookup(nodeId2);

        node1.addNeighbor(node2);
        node2.addNeighbor(node1);
    }

    /**
     * Starts travesing the {@code {@link Network}} at node {@code start} and determines if it's
     * directly or indirectly connection connected to node {@code target}.
     *
     * @param start number of start node
     * @param target number of target node
     * @return {@code true} if node {@code start} is connected to node {@code target}
     */
    public boolean query(int start, int target) {
        checkBounds(start, target);

        // Return early if start and target nodes are directly connected
        Node startNode = lookup(start);
        Node targetNode = lookup(target);
        if (startNode.hasNeighbor(targetNode)) {
            return true;
        }

        // Initialize BFS
        Queue<Node> nodeQueue = new LinkedList<>();
        Set<Node> nodesVisited = new LinkedHashSet<>();
        nodeQueue.add(startNode);

        while (!nodeQueue.isEmpty()) {
            // If the currently visited node is the target, it's connected to the start node
            Node currentNode = nodeQueue.poll();
            if (currentNode.equals(targetNode)) {
                return true;
            }

            // Visit all neighboring nodes along the edges
            for (Node neighbor : currentNode.neighbors) {
                if (!nodesVisited.contains(neighbor)) {
                    nodesVisited.add(neighbor);
                    nodeQueue.add(neighbor);
                }
            }
        }

        return false;
    }

    private void checkBounds(int node1, int node2) {
        if (node1 <= 0 || node1 > nodes.length) {
            throw new IllegalArgumentException("first argument " + node1 + " is out of bounds");
        }

        if (node2 <= 0 || node2 > nodes.length) {
            throw new IllegalArgumentException("second argument " + node2 + " is out of bounds");
        }
    }

    private Node lookup(int nodeId) {
        return nodes[nodeId - 1];
    }

    private class Node {

        final int nodeId;
        final Set<Node> neighbors = new LinkedHashSet<>();

        Node(int nodeId) {
            this.nodeId = nodeId;
        }

        void addNeighbor(Node neighbor) {
            neighbors.add(neighbor);
        }

        boolean hasNeighbor(Node neighbor) {
            return neighbors.contains(neighbor);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }

            if (o == null || !(o instanceof Node)) {
                return false;
            }

            Node node = (Node) o;
            return nodeId == node.nodeId;
        }

        @Override
        public int hashCode() {
            return Objects.hash(nodeId);
        }

    }

}
